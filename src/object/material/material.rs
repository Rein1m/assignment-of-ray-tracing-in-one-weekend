use crate::{ray::Ray, vec3::Color};

use super::super::HitRecord;

pub trait Material {
    fn scatter(&self, ray: &Ray, rec: &HitRecord) -> Option<(Ray, Color)>;
}
