use crate::ray::Ray;

use super::super::HitRecord;

pub trait Hittable {
    fn hit_by(&self, ray: &Ray) -> Option<HitRecord>;
}
