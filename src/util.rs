use image::Rgb;

use crate::vec3::{Color, Point3, Vec3};

pub fn pixel_from(color: Color) -> Rgb<u8> {
    let r = (color.0.clamp(0.0, 0.999).sqrt() * 255.999) as u8;
    let g = (color.1.clamp(0.0, 0.999).sqrt() * 255.999) as u8;
    let b = (color.2.clamp(0.0, 0.999).sqrt() * 255.999) as u8;
    Rgb([r, g, b])
}

pub fn random_in_unit_sphere() -> Vec3 {
    loop {
        let p = Point3::random_in(-1.0, 1.0);
        if p.len_squared() >= 1.0 {
            continue
        }
        return p
    }
}

pub fn random_in_unit_disk() -> Vec3 {
    let random = || -1.0 + 2.0 * rand::random::<f64>();
    loop {
        let p = Point3::new(random(), random(), 0.0);
        if p.len_squared() >= 1.0 {
            continue
        }
        return p
    }
}