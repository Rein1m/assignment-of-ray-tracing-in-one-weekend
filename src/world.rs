use crate::{
    object::Hittable,
    ray::Ray,
    vec3::Color,
};

pub struct World {
    calc_depth: usize,
    hittables: Vec<Box<dyn Hittable>>,
}

impl World {
    pub fn new(calc_depth: usize) -> Self {
        World {
            calc_depth,
            hittables: vec![],
        }
    }

    pub fn add<H: Hittable + 'static>(mut self, hittable: H) -> Self {
        self.hittables.push(Box::new(hittable));
        self
    }

    pub fn get_color(&self, ray: &Ray) -> Color {
        self._get_color(ray, self.calc_depth)
    }

    fn _get_color(&self, ray: &Ray, depth: usize) -> Color {
        if depth <= 0 {
            return Color::new(0.0, 0.0, 0.0);
        }
        let mut min_dist = f64::MAX;
        let mut record = None;
        for hittable in &self.hittables {
            if let Some(rec) = hittable.hit_by(ray) {
                if rec.dist > 0.001 && rec.dist < min_dist {
                    min_dist = rec.dist;
                    record = Some(rec);
                }
            }
        }
        if let Some(rec) = record {
            return if let Some((scattered, atten)) = rec.mat.scatter(ray, &rec) {
                atten * self._get_color(&scattered, depth - 1)
            } else {
                Color::new(0.0, 0.0, 0.0)
            };
        }
        let t = 0.5 * ray.direction.unit().1 + 1.0;
        (1.0 - t) * Color::new(0.7, 0.7, 0.7) + t * Color::new(0.7, 0.4, 0.2)
    }
}
