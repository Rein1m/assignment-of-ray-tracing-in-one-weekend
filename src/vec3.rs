use std::fmt::Debug;
use std::ops::{Add, Mul, Sub};

#[derive(Default, Clone, Copy, Debug)]
pub struct Vec3(pub f64, pub f64, pub f64);

pub type Color = Vec3;
pub type Point3 = Vec3;

impl Vec3 {
    #[inline(always)]
    pub fn new(a: f64, b: f64, c: f64) -> Self {
        Self(a, b, c)
    }

    #[inline(always)]
    pub fn from_tuple((a, b, c): (f64, f64, f64)) -> Self {
        Self(a, b, c)
    }

    pub fn refract(self, normal: Self, etai_over_etat: f64) -> Self {
        let cos_theta = (-self).dot(normal).min(1.0);
        let r_out_perp = etai_over_etat * (self + cos_theta * normal);
        let r_out_parallel = -(1.0 - r_out_perp.len_squared()).abs().sqrt() * normal;
        r_out_perp + r_out_parallel
    }

    #[inline(always)]
    pub fn reflect(self, normal: Self) -> Self {
        self - 2.0 * self.dot(normal) * normal
    }

    #[inline(always)]
    pub fn near_zero(self) -> bool {
        const ZERO: f64 = 1e-6;
        self.0 < ZERO && self.1 < ZERO && self.2 < ZERO
    }

    #[inline(always)]
    pub fn random() -> Vec3 {
        use rand::random;
        Self(random::<f64>(), random::<f64>(), random::<f64>())
    }

    #[inline(always)]
    pub fn random_in(min: f64, max: f64) -> Vec3 {
        let random = || min + (max - min) * rand::random::<f64>();
        Self(random(), random(), random())
    }

    #[inline(always)]
    pub fn dot(self, rhs: Self) -> f64 {
        self.0 * rhs.0 + self.1 * rhs.1 + self.2 * rhs.2
    }

    #[inline(always)]
    pub fn cross(self, rhs: Self) -> Self {
        Vec3(
            self.1 * rhs.2 - self.2 * rhs.1,
            self.0 * rhs.2 - self.2 * rhs.0,
            self.0 * rhs.1 - self.1 * rhs.0,
        )
    }

    #[inline(always)]
    pub fn len_squared(self) -> f64 {
        self.0 * self.0 + self.1 * self.1 + self.2 * self.2
    }

    #[inline(always)]
    pub fn len(self) -> f64 {
        (self.len_squared() as f64).sqrt() as f64
    }

    #[inline(always)]
    pub fn unit(self) -> Self {
        self / self.len()
    }
}

macro_rules! binop {
    ($a: ident :: $am: ident , $o: ident:: $om: ident) => {
        impl $o<Vec3> for Vec3 {
            type Output = Self;

            #[inline(always)]
            fn $om(self, rhs: Self) -> Self::Output {
                let a = self.0.$om(rhs.0);
                let b = self.1.$om(rhs.1);
                let c = self.2.$om(rhs.2);
                Vec3(a, b, c)
            }
        }

        impl std::ops::$a for Vec3 {
            #[inline(always)]
            fn $am(&mut self, rhs: Self) {
                *self = self.$om(rhs);
            }
        }
    };
}

binop!(AddAssign::add_assign, Add::add);
binop!(MulAssign::mul_assign, Mul::mul);
binop!(SubAssign::sub_assign, Sub::sub);

impl std::ops::MulAssign<f64> for Vec3 {
    #[inline(always)]
    fn mul_assign(&mut self, rhs: f64) {
        *self = rhs * *self;
    }
}

impl std::ops::Mul<Vec3> for f64 {
    type Output = Vec3;

    #[inline(always)]
    fn mul(self, rhs: Vec3) -> Self::Output {
        Vec3(self * rhs.0, self * rhs.1, self * rhs.2)
    }
}

impl std::ops::Mul<f64> for Vec3 {
    type Output = Self;

    #[inline(always)]
    fn mul(self, rhs: f64) -> Self::Output {
        rhs * self
    }
}

impl std::ops::Div<f64> for Vec3 {
    type Output = Self;

    #[inline(always)]
    fn div(self, rhs: f64) -> Self::Output {
        (1.0 / rhs) * self
    }
}

impl std::ops::DivAssign<f64> for Vec3 {
    #[inline(always)]
    fn div_assign(&mut self, rhs: f64) {
        *self = *self / rhs;
    }
}

impl std::ops::Neg for Vec3 {
    type Output = Self;

    #[inline(always)]
    fn neg(self) -> Self::Output {
        Self(-self.0, -self.1, -self.2)
    }
}
