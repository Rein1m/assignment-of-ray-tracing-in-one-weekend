use crate::{
    ray::Ray,
    vec3::{Point3, Vec3},
};

use super::Material;

pub struct HitRecord<'a> {
    pub dist: f64,
    pub point: Point3,
    pub normal: Vec3,
    pub front_face: bool,
    pub mat: &'a dyn Material,
}

pub struct HitRecordBuilder<'a> {
    dist: f64,
    point: Point3,
    normal: Option<Vec3>,
    front_face: bool,
    mat: &'a dyn Material,
}

impl<'a> HitRecordBuilder<'a> {
    pub fn new(dist: f64, point: Point3, mat: &'a dyn Material) -> Self {
        Self {
            point,
            front_face: false,
            dist,
            normal: None,
            mat,
        }
    }
    pub fn set_normal(mut self, ray: &Ray, outward_normal: Vec3) -> Self {
        self.front_face = ray.direction.dot(outward_normal) < 0.0;
        self.normal = match self.front_face {
            true => Some(outward_normal),
            false => Some(-outward_normal),
        };
        self
    }
    pub fn build(self) -> HitRecord<'a> {
        HitRecord {
            dist: self.dist,
            point: self.point,
            normal: self.normal.unwrap(),
            front_face: self.front_face,
            mat: self.mat
        }
    }
}
