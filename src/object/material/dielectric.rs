use crate::{
    object::{HitRecord, Material},
    ray::Ray,
    vec3::Color,
};

pub struct Dielectric {
    ir: f64,
}

impl Dielectric {
    pub fn new(ir: f64) -> Self {
        Self { ir }
    }
}

impl Material for Dielectric {
    fn scatter(&self, ray: &Ray, rec: &HitRecord) -> Option<(Ray, Color)> {
        let refraction_ratio = if rec.front_face {
            1.0 / self.ir
        } else {
            self.ir
        };
        let refracted = ray.direction.unit().refract(rec.normal, refraction_ratio);
        Some((Ray::new(rec.point, refracted), Color::new(1.0, 1.0, 1.0)))
    }
}
