pub mod hittable;
pub use hittable::Hittable;

mod record;
pub use record::*;

pub mod material;
pub use material::Material;